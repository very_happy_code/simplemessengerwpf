﻿using System;
using System.Windows;


namespace ServerAppWpf
{
    public partial class ServerMainWindow : Window
    {
        private readonly c_ServerViewModel ro_serverViewModel;


        public ServerMainWindow()
        {
            InitializeComponent();

            var ip_setup_dialog = new SetupIPDialog();

            if (ip_setup_dialog.ShowDialog() == true) {
                ro_serverViewModel = new c_ServerViewModel(ip_setup_dialog.pr_sIP);
            }
            else {
                this.Close();
            }

            DataContext = ro_serverViewModel;
        }
    }
}
