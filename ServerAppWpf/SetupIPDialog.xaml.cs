﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ServerAppWpf
{
    /// <summary>
    /// Interaction logic for SetupIPDialog.xaml
    /// </summary>
    public partial class SetupIPDialog : Window
    {
        public string pr_sIP => IPTextbox.Text;

        private void Accept_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        public SetupIPDialog()
        {
            InitializeComponent();
        }
    }
}
