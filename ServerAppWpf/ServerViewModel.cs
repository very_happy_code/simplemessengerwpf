﻿using global::System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Windows.Media;
using System.Windows.Input;

using MyMessenger;


namespace ServerAppWpf
{
    internal class c_ServerViewModel : INotifyPropertyChanged
    {
        private const string cn_sClientDisconnectedStatus = "Client status: DISCONNECTED";
        private const string cn_sClientConnectedStatus = "Client status: Connected";

        private readonly c_MyMessengerServer ro_server;
        private readonly SynchronizationContext ro_context = SynchronizationContext.Current;
        private c_SendMessageCommand sendMessageCommand;
        private string _sMessageText = null;


        public ObservableCollection<c_MessageItem> pr_MessageLog { get; } = new ObservableCollection<c_MessageItem>();
        public string pr_sClientStatus { get; private set; }
        public SolidColorBrush pr_ClientStatusTextColor { get; private set; }
        public c_SendMessageCommand pr_SendMessageCommand
        {
            get {
                return sendMessageCommand ?? (sendMessageCommand = new c_SendMessageCommand(ro_server, ro_context));
            }
        }
        public string pr_sMessageText
        {
            get => _sMessageText;
            set {
                _sMessageText = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(pr_sMessageText)));
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;


        private void selectClientStatus()
        {
            if (ro_server.pr_bClientConnected) {
                pr_sClientStatus = cn_sClientConnectedStatus;
                pr_ClientStatusTextColor = Brushes.DarkGreen;
            }
            else {
                pr_sClientStatus = cn_sClientDisconnectedStatus;
                pr_ClientStatusTextColor = Brushes.DarkRed;
            }
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(pr_sClientStatus)));
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(pr_ClientStatusTextColor)));
        }

        private void serverConnectionStatusChangedEventHandler()
        {
            selectClientStatus();

            //if connection with client lost, wait for new client:
            if (!ro_server.pr_bClientConnected) {
                ro_server.AcceptClientAsync();
            }
        }
        private void serverMessageReceivedEventHandler(string message)
        {
            ro_context.Send(state => pr_MessageLog.Add(new c_MessageItem("client:", message)), state: null);
        }
        private void serverMessageSendedEventHandler(string message)
        {
            ro_context.Send(state => pr_MessageLog.Add(new c_MessageItem("server:", message + '\n')), state: null);
            ro_context.Send(start => pr_sMessageText = String.Empty, state: null);
        }


        public c_ServerViewModel(string IP)
        {
            this.ro_server = new c_MyMessengerServer(IP);

            ro_server.ev_ConnectionStatusChanged += serverConnectionStatusChangedEventHandler;
            ro_server.ev_MessageReceived += serverMessageReceivedEventHandler;
            ro_server.ev_MessageSended += serverMessageSendedEventHandler;

            selectClientStatus();    //setup initial value for pr_sClientStatus
            ro_server.AcceptClientAsync();  //start waiting for client
        }
    }


    internal class c_MessageItem
    {
        public string pr_sSender { get; }
        public string pr_sMessage { get; }


        public c_MessageItem(string sender, string message)
        {
            this.pr_sSender = sender;
            this.pr_sMessage = message;
        }
    }


    internal class c_SendMessageCommand : ICommand
    {
        private readonly c_MyMessengerServer ro_server;


        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter) => ro_server.pr_bClientConnected;

        public void Execute(object parameter) => ro_server.SendMessageToClient(parameter as string);

        public c_SendMessageCommand(c_MyMessengerServer server, SynchronizationContext context)
        {
            this.ro_server = server;
            ro_server.ev_ConnectionStatusChanged += () => context.Send(state => CanExecuteChanged?.Invoke(this, null), state: null);
        }
    }
}