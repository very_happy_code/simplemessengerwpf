﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientAppWpf
{
    /// <summary>
    /// Interaction logic for ClientMainWindow.xaml
    /// </summary>
    public partial class ClientMainWindow : Window
    {
        private readonly c_ClientViewModel ro_clientViewModel;


        public ClientMainWindow()
        {
            InitializeComponent();

            var ip_setup_dialog = new SetupIPDialog();

            if (ip_setup_dialog.ShowDialog() == true) {
                ro_clientViewModel = new c_ClientViewModel(ip_setup_dialog.pr_sIP);
            }
            else {
                this.Close();
            }

            DataContext = ro_clientViewModel;
        }
    }
}
