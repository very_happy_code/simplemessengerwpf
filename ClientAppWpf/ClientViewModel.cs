﻿using global::System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Windows.Media;
using System.Windows.Input;

using MyMessenger;


namespace ClientAppWpf
{
    internal class c_ClientViewModel : INotifyPropertyChanged
    {
        private const string cn_sServerStatusOffline = "Server status: OFFLINE";
        private const string cn_sServerStatusOnline = "Server status: Online";

        private readonly c_MyMessengerClient ro_client;
        private readonly SynchronizationContext ro_context = SynchronizationContext.Current;
        private c_SendMessageCommand sendMessageCommand;
        private string _sMessageText;


        public ObservableCollection<c_MessageItem> pr_MessageLog { get; } = new ObservableCollection<c_MessageItem>();
        public string pr_sServerStatus { get; private set; }
        public SolidColorBrush pr_ServerStatusTextColor { get; private set; }
        public c_SendMessageCommand pr_SendMessageCommand
        {
            get {
                return sendMessageCommand ?? (sendMessageCommand = new c_SendMessageCommand(ro_client, ro_context));
            }
        }
        public string pr_sMessageText
        {
            get => _sMessageText;
            set {
                _sMessageText = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(pr_sMessageText)));
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;


        private void selectServerStatus()
        {
            if (ro_client.pr_bConnectedToServer) {
                pr_sServerStatus = cn_sServerStatusOnline;
                pr_ServerStatusTextColor = Brushes.DarkGreen;
            }
            else {
                pr_sServerStatus = cn_sServerStatusOffline;
                pr_ServerStatusTextColor = Brushes.DarkRed;
            }
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(pr_sServerStatus)));
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(pr_ServerStatusTextColor)));
        }

        private void clientConnectionStatusChangedEventHandler()
        {
            selectServerStatus();

            if (!ro_client.pr_bConnectedToServer) {
                ro_client.StartConnectionAsync();
            }
        }
        private void clientMessageReceivedEventHandler(string message)
        {
            ro_context.Send(state => pr_MessageLog.Add(new c_MessageItem("server:", message)), state: null);
        }
        private void clientMessageSendedEventHandler(string message)
        {
            ro_context.Send(state => pr_MessageLog.Add(new c_MessageItem("client:", message + '\n')), state: null);
            ro_context.Send(start => pr_sMessageText = String.Empty, state: null);
        }


        public c_ClientViewModel(string IP)
        {
            this.ro_client = new c_MyMessengerClient(IP);

            ro_client.ev_ConnectionStatusChanged += clientConnectionStatusChangedEventHandler;
            ro_client.ev_MessageReceived += clientMessageReceivedEventHandler;
            ro_client.ev_MessageSended += clientMessageSendedEventHandler;

            selectServerStatus();
            ro_client.StartConnectionAsync();
        }
    }


    internal class c_MessageItem
    {
        public string pr_sSender { get; }
        public string pr_sMessage { get; }


        public c_MessageItem(string sender, string message)
        {
            this.pr_sSender = sender;
            this.pr_sMessage = message;
        }
    }


    internal class c_SendMessageCommand : ICommand
    {
        private readonly c_MyMessengerClient ro_client;


        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter) => ro_client.pr_bConnectedToServer;

        public void Execute(object parameter) => ro_client.SendMessageToServer(parameter as string);

        public c_SendMessageCommand(c_MyMessengerClient client, SynchronizationContext context)
        {
            this.ro_client = client;
            ro_client.ev_ConnectionStatusChanged += () => context.Send(state => CanExecuteChanged?.Invoke(this, null), state: null);
        }
    }
}