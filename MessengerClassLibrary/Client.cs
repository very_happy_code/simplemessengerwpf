﻿using global::System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Diagnostics;


namespace MyMessenger
{
    public class c_MyMessengerClient : ac_MyMessengerBase
    {
        private readonly IPAddress ro_serverAddress;
        private readonly IPEndPoint ro_serverEndPoint;

        private Socket connectionSocket;
        private bool _bConnectedToServer = false;


        public bool pr_bConnectedToServer
        {
            get => _bConnectedToServer;
            set {
                if (value != _bConnectedToServer) {
                    _bConnectedToServer = value;
                    ev_ConnectionStatusChanged?.Invoke();
                }
                if (value)
                    signalConnectionEstablished();
                else
                    signalConnectionLost();
            }
        }


        public event Action ev_ConnectionStatusChanged;


        public async Task ConnectToServerAsync()
        {
            if (connectionSocket != null) {
                TryShutdownConnection();    //ignore exceptions on shutdown
            }

            connectionSocket = new Socket(ro_serverAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            try {
                await connectionSocket.ConnectAsync(ro_serverEndPoint);

                pr_bConnectedToServer = true;

                startReceiveMessages(connectionSocket);
            }
            catch (Exception ex) {
                //TODO: make error handling here
                Debug.WriteLine("Client: exception while trying to establish connection with server; Exception: " + ex.Message);
            }
        }
        public async void StartConnectionAsync()
        {
            while (true) {
                await ConnectToServerAsync();

                if (!pr_bConnectedToServer) {
                    await Task.Delay(1000);
                }
                else {
                    break;
                }
            }
        }
        public void SendMessageToServer(string message)
        {
            if (message == null || message == String.Empty) {
                return;
            }

            //update connection status:
            pr_bConnectedToServer = connectionSocket?.CheckConnectionStatusExt() ?? false;

            if (!pr_bConnectedToServer) {
                //TODO: handle
                Debug.WriteLine("Client: can't send message (no connection to server)");
                return;
            }

            try {
                sendMessage(connectionSocket, message);
            }
            catch(Exception ex) {
                //TODO: handle sending exception
                Debug.WriteLine("Client: Exception while sending message. Exception: " + ex.Message);
            }
        }
        public Exception TryShutdownConnection()
        {
            Exception exception = null;

            try {
                connectionSocket?.Shutdown(SocketShutdown.Both);
            }
            catch (Exception ex) {
                exception = ex;
            }

            connectionSocket?.Close();
            connectionSocket = null;

            pr_bConnectedToServer = false;

            return exception;
        }


        private void breakReceivingCallback() => TryShutdownConnection();


        public static c_MyMessengerClient st_CreateClientUsingFirstLocalAddress()
        {
            IPHostEntry host_entry = Dns.GetHostEntry(Dns.GetHostName());
            return new c_MyMessengerClient(host_entry.AddressList[0]);
        }
        public static c_MyMessengerClient st_CreateClientUsingLoopbackAddress()
        {
            return new c_MyMessengerClient(IPAddress.Loopback);
        }


        public c_MyMessengerClient(IPAddress serverAddress)
            : base(c_MyMessengerServer.st_ro_ServerEncoding)
        {
            if (serverAddress == null) {
                throw new ArgumentNullException(nameof(serverAddress));
            }
            else if (serverAddress == IPAddress.None) {
                throw new ArgumentException($"{nameof(serverAddress)} = {serverAddress} (IPAddress.None)");
            }

            base.m_dlBreakReceivingCallback = breakReceivingCallback;

            this.ro_serverAddress = serverAddress;
            this.ro_serverEndPoint = new IPEndPoint(serverAddress, c_MyMessengerServer.cn_nServerEndPointPort);
        }
        public c_MyMessengerClient(string IP)
            : this(IPAddress.Parse(IP))
        { }
    }
}