﻿using global::System;
using System.Net.Sockets;
using System.Text;
using System.Threading;


namespace MyMessenger
{
    public abstract class ac_MyMessengerBase
    {
        private const int cn_nTransmissionBufferLength = 100;

        private readonly Encoding ro_encoding;
        private readonly byte[] ro_transmissionBuffer = new byte[cn_nTransmissionBufferLength];
        private readonly StringBuilder ro_messageBuilder = new StringBuilder();
        private readonly ManualResetEvent ro_connectionResetEvent = new ManualResetEvent(initialState: false);

        protected Action m_dlBreakReceivingCallback;


        public event Action<string> ev_MessageReceived;
        public event Action<string> ev_MessageSended;


        public void WaitForConnection(int millisecondsTimeout = -1)
        {
            ro_connectionResetEvent.WaitOne(millisecondsTimeout);
        }


        protected void signalConnectionEstablished() => ro_connectionResetEvent.Set();
        protected void signalConnectionLost() => ro_connectionResetEvent.Reset();
        protected void sendMessage(Socket socket, string message)
        {
            if (!socket.CheckConnectionStatusExt()) {
                throw new Exception("Socket is not connected!");
            }

            string[] strings = message.Split('\n');
            byte[] bytes;

            for (int i = 0; i < strings.Length; ++i) {
                if (strings[i].Length == 0) {
                    continue;
                }
                bytes = ro_encoding.GetBytes(strings[i] + "\n");

                socket.Send(bytes);
            }

            ev_MessageSended?.Invoke(message);
        }
        protected void startReceiveMessages(Socket socket)
        {
            try {
                socket.BeginReceive(ro_transmissionBuffer, offset: 0, cn_nTransmissionBufferLength,
                    SocketFlags.None, new AsyncCallback(receiveCallback), state: socket);
            }
            catch {
                m_dlBreakReceivingCallback?.Invoke();
            }
        }

        private void receiveCallback(IAsyncResult result)
        {
            var socket = result.AsyncState as Socket;

            int bytes_received = 0;

            try {
                bytes_received = socket.EndReceive(result);
            }
            catch { /* noop */ }

            if (bytes_received == 0) {
                m_dlBreakReceivingCallback?.Invoke();
                return;
            }

            ro_messageBuilder.Append(ro_encoding.GetString(ro_transmissionBuffer, index: 0, bytes_received));

            if (ro_messageBuilder.Length > 0) {
                //if receive full line:
                if (ro_messageBuilder[ro_messageBuilder.Length - 1] == '\n') {
                    string message = ro_messageBuilder.ToString();
                    ev_MessageReceived?.Invoke(message);

                    ro_messageBuilder.Clear();
                }
            }

            //continue receiving loop:
            startReceiveMessages(socket);
        }


        protected ac_MyMessengerBase(Encoding encoding)
        {
            this.ro_encoding = encoding;
        }
    }
}