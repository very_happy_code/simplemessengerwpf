﻿using global::System;
using System.Net.Sockets;


namespace MyMessenger
{
    public static class sc_SocketExtension
    {
        public static bool CheckConnectionStatusExt(this Socket socket)
        {
            if (!socket.Connected) {
                return false;
            }
            else {
                bool can_write = socket.Poll(microSeconds: 10_000, SelectMode.SelectWrite);
                bool have_available_data = socket.Available > 0;

                return can_write || have_available_data;
            }
        }
    }
}