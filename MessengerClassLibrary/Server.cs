﻿using global::System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Diagnostics;


namespace MyMessenger
{
    public class c_MyMessengerServer : ac_MyMessengerBase
    {
        internal const int cn_nServerEndPointPort = 10000;

        public static readonly Encoding st_ro_ServerEncoding = Encoding.Unicode;

        private readonly IPAddress ro_serverAddress;
        private readonly IPEndPoint ro_localEndPoint;

        private Socket serverSocket;
        private Socket clientConnectionSocket = null;
        private volatile bool _bClientConnected = false;


        public bool pr_bClientConnected
        {
            get => _bClientConnected;
            set {
                lock (ro_localEndPoint) {
                    if (_bClientConnected != value) {
                        _bClientConnected = value;
                        ev_ConnectionStatusChanged?.Invoke();
                    }
                    if (value)
                        signalConnectionEstablished();
                    else
                        signalConnectionLost();
                }
            }
        }


        public event Action ev_ConnectionStatusChanged;


        public async void AcceptClientAsync()
        {
            if (serverSocket == null) {
                serverSocket = new Socket(ro_serverAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                serverSocket.Bind(ro_localEndPoint);
                serverSocket.Listen(1);
            }

            try {
                clientConnectionSocket = await serverSocket.AcceptAsync();

                pr_bClientConnected = true;

                startReceiveMessages(clientConnectionSocket);
            }
            catch (Exception ex) {
                //TODO: handle exception
                Debug.WriteLine("Server: exception while waiting for client. Exception: " + ex.Message);
            }
        }

        public void SendMessageToClient(string message)
        {
            if (message == null || message == String.Empty) {
                return;
            }

            pr_bClientConnected = clientConnectionSocket?.CheckConnectionStatusExt() ?? false;

            if (!pr_bClientConnected) {
                //TODO: handle
                Debug.WriteLine("Server: can't send message (no connection to client)");
                return;
            }

            try {
                sendMessage(clientConnectionSocket, message);
            }
            catch(Exception ex) {
                //TODO: handle sending exception
                Debug.WriteLine("Server: Exception while sending message. Exception: " + ex.Message);
            }
        }
        public void BreakConnectionWithClient()
        {
            clientConnectionSocket?.Shutdown(SocketShutdown.Both);
            clientConnectionSocket?.Close();
            clientConnectionSocket = null;

            pr_bClientConnected = false;
        }


        private void breakReceivingCallback() => BreakConnectionWithClient();


        public static c_MyMessengerServer st_CreateServerUsingFirstLocalAddress()
        {
            IPHostEntry host_entry = Dns.GetHostEntry(Dns.GetHostName());
            return new c_MyMessengerServer(host_entry.AddressList[0]);
        }
        public static c_MyMessengerServer st_CreateServerUsingLoopbackAddress()
        {
            return new c_MyMessengerServer(IPAddress.Loopback);
        }


        public c_MyMessengerServer(IPAddress serverAddress)
            : base(st_ro_ServerEncoding)
        {
            if (serverAddress == null) {
                throw new ArgumentNullException(nameof(serverAddress));
            }
            else if (serverAddress == IPAddress.None) {
                throw new ArgumentException($"{nameof(serverAddress)} = {serverAddress} (IPAddress.None)");
            }

            base.m_dlBreakReceivingCallback = breakReceivingCallback;

            this.ro_serverAddress = serverAddress;
            this.ro_localEndPoint = new IPEndPoint(serverAddress, c_MyMessengerServer.cn_nServerEndPointPort);
        }
        public c_MyMessengerServer(string IP)
            : this(IPAddress.Parse(IP))
        { }
    }
}