# About

Simple client-server application allowing network text messaging. Application use WPF for UI, and TCP socket for network.

* Server designed to communicate with only 1 client at a time. To establish connection with other client, you must disconnect previous client first.
* Both server and client displays connection status (is server online/is client connected).
* Both client and server uses MVVM pattern and data bindings.
* App will reestablish connection automatically if server or client will be disconnected.